import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

    @Input() postTitle: string;
    @Input() postText: string;
    @Input() postDate: date;
    @Input() likeIt: number;

    likeIt = 0;

    clickLike() :void {
        this.likeIt++;
    }

    clickDislike() :void {
        this.likeIt--;
    }

  constructor() { }

  ngOnInit() {
  }

}
